const DEFAULT_DOC = "-> Click a document from below to load it into the box <-"

function renderContent(content) { // Thanks to Showdown docs for this function's innards
    var converter = new showdown.Converter({tables: true});
    return converter.makeHtml(content);
}

function initaliseBox(name, content, see) {
    see = see || false;

    if (see){
        getDocuments()

        // Thanks to ChatGPT for helping me figure out this code
        const foundDocument = docs.filter((doc) => {
            return doc.name === (content)
        });

        content = foundDocument[0].content || "error, probably caused by developers please tell them off"
    }

    $("#docTitle").text(name)

    $("#box")
        .css({
            "width": "100%",
            "min-height": "50vh",
            "border": "solid",
            "padding": "2.5%"
        })
        .append($("<p>").text(content))
        .html(renderContent(content.replace(/¿¿putabacktickhere/gm, "\`")))

    $("blockquote").css({
        "background-color" : "#dee0de",
        "margin-left":"1em",
        "padding":"0.5em"
    })

    $("table").addClass("table")

    putInURL("document", name)

}


function goToList() {
    $(window).scrollTo($("#listArea"), 200)
}

function makeUsSearch(query){
    $("#searchBox").val(query);
    $("#search").trigger("click");
    goToList()
}

function formatTags(tagList) {
    var html = "<code>"; // Add missing 'var' keyword to declare the variable

    tagList.sort(); // To make it easier to read

    for (var i = 0; i < tagList.length; i++) { // Add missing 'var' keyword to declare the loop variable
        var tagName = tagList[i];

        html = html + "<span title=\"Click on a tag to search for it!\" class=\"badge bg-secondary\" onclick=\"makeUsSearch('" + tagName + "')\">" + tagName + "</span>&nbsp;";
    }

    html = html + "</code>";

    return html;
}

function handleUserInput(){
    userText = $("#placeForText").val()
    initaliseBox("User input", userText, false)
    putInURL("userText", userText)
    deleteArgFromURL("document")
}

function userArea(on) {
    if (on){
        value = getFromURL("userText") 
        
        if (value.length == 0) {
            value = "Write some markdown here!"
        }

        $("#userArea")
            .append(
                $("<textarea>")
                    .val(value)
                    .attr("id", "placeForText")
                    .on("input", () => {
                        handleUserInput()
                    })
                    .css({
                        "width": "100%",
                        "height":"25vh",
                        "border":"solid",
                        "border-style":"dashed",
                        "padding":"2.5%"
                    })
            )
    } else {
        $("userArea")
            .empty()
    }
}

function addNoobButtons() {
    $("#buttonArea")
      .addClass("justify-content-center")
      .append(
        $("<button>")
          .text("See all ideas and inventions")
          .addClass("btn btn-primary m-3")
          .click(() => {
            makeUsSearch("ideas")
          }),
        $("<button>")
          .text("See all blog posts")
          .addClass("btn btn-primary m-3")
          .click(() => {
            makeUsSearch("blogbits")
          }),
        $("<button>")
        .text("Read microstories")
        .addClass("btn btn-primary m-3")
        .click(() => {
            makeUsSearch("microstories")
            }),
        $("<button>")
          .text("Go to default page")
          .addClass("btn btn-primary m-3")
          .click(() => {
            initaliseBox(DEFAULT_DOC, DEFAULT_DOC, true);
          })
      );
}

function populateList(docs) {
    $("#writtenTable").remove();

    var forTable = [
        ["<p title=\"The document you wish to open\">Name</p>", "<p title=\"What the document is about\">Description</p>", "<p title=\"What classifies the document?\">Tags</p>"],
    ];

    for (var i = 0; i < docs.length; i++) { // Add missing 'var' keyword to declare the loop variable
        const nameLink =
            '<p style="color: blue;" title="Press on the document to load it into the box" class="scroll-to-top" onclick="initaliseBox(\'' +
            docs[i].name +
            "', `" +
            docs[i].name +
            '`, true); userArea(false)">' +
            docs[i].name +
            '</p>';

        forTable.push([nameLink, docs[i].description, formatTags(docs[i].tags)]);
    }

    $("#listArea").append(writeTable(forTable));

    // Thanks to ChatGPT for helping me scroll to the top
    $('.scroll-to-top').click(function() {
        $('html, body').animate({scrollTop: 0 }, 'fast');
        return false;
      });
}

function addSearch(docs) {
    $("#listArea").append(`
        <div class="input-group">
            <input type="text" id="searchBox" class="form-control" placeholder="Enter in a space-seperated list of tags to search for">
            <button id="search" class="btn btn-outline-secondary">Search!</button>
        </div>
    `);

    $("#search").click(function () {
        // Thanks to ChatGPT for helping me search for tags!
        var search = $("#searchBox").val();
        putInURL("search", search)

        var searchedDocs = docs

        if (search.length != 0) {
            const searchFor = search.split(" ");

            searchedDocs = docs.filter((doc) => {
                return searchFor.every((tag) => doc.tags.includes(tag));
            });
        }

        populateList(searchedDocs);
    });
}


function checkArgument(){
    // Handler for the document value
    documentValue = getFromURL("document")

    if (documentValue.length > 0 && documentValue == "User input") {
        initaliseBox(DEFAULT_DOC, DEFAULT_DOC, true);
    } else if (documentValue.length > 0 && documentValue != DEFAULT_DOC){
        initaliseBox(documentValue, documentValue, true)
    } else {
        initaliseBox(DEFAULT_DOC, DEFAULT_DOC, true);
    }

    // userArea inputs
    if ($("#placeForText").val() != "Write some markdown here!") {
        handleUserInput()
    }

    // SearchBox Entries
    $("#searchBox").val(getFromURL("search"))
    $("#search").trigger("click")

}

var docs = getDocuments().sort((a, b) => {
    // Thanks to ChatGPT for generating this sorting code

    const nameA = a.name.toLowerCase(); // Convert to lowercase for case-insensitive sorting
    const nameB = b.name.toLowerCase();
    if (nameA < nameB) {
      return -1; // 'a' comes before 'b'
    }
    if (nameA > nameB) {
      return 1; // 'b' comes before 'a'
    }
    return 0; // names are equal
});


$("#contentArea").append(
    $("<p>").text("If you are reading these documents on the internet as published by 56independent/Vitra Suchovich only, permission is granted to read these freely. Use the license provided from the documents when possible. Failing that, use the following license:"),
    $('<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><'),
    $("<h2>")
        .attr("id", "docTitle"),
    $("<div>")
        .attr("id", "box")
);

addSearch(docs);
populateList(docs);
userArea(true)
checkArgument()
addNoobButtons()
