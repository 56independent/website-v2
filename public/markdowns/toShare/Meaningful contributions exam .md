# Meaningful contributions exam board
Introduction
------------

I have an idea for an exam board which focuses not on what students can do in a closed, sterile environment, but more rather what they can do in the real world and how they can communicate with others like they will in real life.

Assessment
----------

There is no “final exam” given to students at which point they then are free to forget what they learnt; instead, coursework assignments are given, forcing students to perform meaningful contributions for meaningful grades. These contributions are done over a long period, thus increasing the time spent working on it and the amount remembered. Since work done more closely follows real-world work, there is a motivation to work harder.

Learning
--------

Learning how to do the coursework is given by teachers during “project time”. No contributions may be made until each learning time is finished. This ensures that students who already know how are not given an unfair advantage.

Standardisation
---------------

Every candidate is made equal by most exam boards. The same must be done here.

Candidates are only allowed to spend time in after-school “project time”, spent working on the projects. Anything added outside of “project time” will not be counted unless an equal amount of “project time” is lost. Since every candidate has the same amount of time, they should have the same advantage.

“Project time” should be equal across schools; that is, three weekdays have “project time” after school, where all candidates in the school work together.

Intent
------

The exam board is intended to be used in collaboration with another exam board, as a form of "extra credit" kind of project.

The exam board allows candidates to use their grades from this exam board alongside traditional GCSE grades, resulting in the ability of choice and portfolio addition.

Benifits
--------

This exam board, unlike many others, focuses on benifiting both the candidate and the world around them.

The candidate benifits from having a project added to their portfolio and grades, whilst the world benifits from having useful contributions given to it.

Choices
-------

Each candidate may choose a maximumm of two subjects; this ensures that skills reflect those that they desire to use in the future.  If two subjects are chosen, students must spend more “project time”, one unit for each subject. 

Transparency
------------

Candidates may see the sheets used for marking their work and review them. Should an issue appear, such as misunderstanding or incorrect marking, they may request an examination of the examination and their specific worries.

Years
-----

Year 10 is spent learning the theory, alongside practice rounds of creation. Prerequisites are also learnt here.

Year 11 is spent performing the coursework and relearning parts of year 10, where actual contribution is made.