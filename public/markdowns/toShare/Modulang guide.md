# Grammar Guide
Introduction
------------

Modulang is a new language designed to combine both isolating and polysynthetic features. The volcabulary features no abstract concepts at its core (but there are a series of mixtures).

It's mainly a personal language to make me happy.

Modulang is in VSO order, and uses the simple concept of modifying nouns to get what you want. 

Phonology
---------

Most of the letters are quite similar to English, but there are a few weird letters that will catch you out:

*   ‘ or tt - Pronounce as a glottal stop, as in the stereotypical British “bu’er”.
*   ñ or ny - Pronounce the n as in “canyon”, without the y
*   ŋ or ng - Pronounce as a "g-infected n" in “nothing”
*   đ or bf - Make your lips a circle and blow for an f-like sound
*   ß or sh - Harsh s as in “harsh”
*   x - As in scottish “loch”
*   ll - As in Spanish “mantequilla”
*   ii - As in Catalan  “illa”
*   ħ - No English equivelent, IPA н. See how it's pronounced.
*   à - As in a of English “awesome”
*   è - As in e of English “elegant”

There are also a series of diacritics available:

*   The acute (ó, can also be O!) accent stresses a syllable, like in the difference between “record” (unstresed e) and “récord” (stressed e)
*   The circumflex (ô, can also be oo) accent lengthens the vowel, like in the difference between "hop” (short o) and hope” (long o)
*   The dieresis (ö, can also be .o) accent makes the pronunciation of that vowel seperate from the previous one
*   The merger (bçf, can also be (bf)) letter will merge the mouth-shapes of two different sounds and ask you to pronounce them together

This language also sometimes uses tones, but luckily they're not too common. Tone markings come after their syllables.

*   \- means a high tone
*   / means a rising tone
*   · or . means a neutral tone
*   \\ means a descending tone
*   \_ means a low tone
*   The absence of a marker represents that any tone may be used.

Building a sentence
-------------------

### Selecting a system

There are two main systems of construction: the synthetic and the isolating system.

The isolating system focuses more on freedom and isolated blocks of meaning. This system is very freeing and allows you to use individual modifiers as little “concept blocks”. For example. the particle for respect, “àf” would usually be added to a noun, but on its own can make a sentence representing the concept of respect. “àf.”. You might even add a verb modifier to make it mean the act of being respectful, like, say “àf az aslle”.

The synthetic system prefers a more rigid system; every word MUST begin with a noun-root. However, this system has more restrictive phonotatics to make a more flowing system of pronunciation. A translation of the isolating “àf az aslle”. might be “iàn-ôv-àf-asié-pase” using the polysynthetic system, which means “person of respect \[verb modifer\]”. Note how the sentence flows better and quicker. This is actually a whole word, but the dashes are used to seperate morphemes to prevent confusion. In some space-saving texts, the dashes may be removed to make “iànôvàfasiépase”, which makes the individual morphemes harder to seperate but the ink costs cheaper. This is similar to how the government of USSR got rid of the definite “вю” and indefinite “ан” articles in russian and replaced them with cases; it moved a constant ink cost to a ink cost to learn cases once. This was much cheaper. 

In most high-grade literature a mix of both systems is used based on what the sentence need; emphasis on choppy blocks or a nice long flowing word?

### Noun roots

Noun roots are what will begin most sentences. All “pure” noun roots will always stand for some simple, concrete concept. “concrete” simply means that you can interact with it physically.

Since the noun roots are concrete, they are also often onomatopoeic. Animal is “mû” after cow sounds, and wheels are “ xoxúpa” or “ đođúa-” after the sounds of the spokes and the tyres. Moving air is  “đouou” due to your mouth movements whilst saying, and water is “u’u' ” after the swallowing sounds made from it.

However, there are quite a few “shortenings”, which are contractions of common combinations leading to shorter sentences. These can encode abstract concepts. Take the word “ârtellé/”, which means alive. It is formed of the three morphemes “ârté", “dell”, and "aßlle”, which literally means "\[heart\]-\[adjective\]-\[to do\]". The slash at the end represents a rising tone. This means multiple contracts with the same letters can be used.

### Tangent: The transcription system

Sitting between the English translation and the modulang sentence is a confusing system of brackets and dashes, representing the morphemes. This is vital for understanding the structure of a sentence and its individual components.

Let's decode it.

*   A square bracket (\[xyz\]) denotes an individual morpheme and what it means
*   A bracket ((xyz)) represents a compond concept forrmed of a few morphemes merged together
*   A dash (-) represents a boundary between morphemes
*   A space ( ) represents a boundary between two words.
*   A underscore around a morpheme group (\_xyz\_) represents optionality

### Adding to the nouns: making verbs and more specific concepts

Once you have a noun root, you can now add to it. We'll build “i love you”

The grammar here is simple. First, you specify what you mean with the noun by adding other nouns or particles if you need. For example, to represent love, you might want to use “\[heart\]-\[for\]-\[person\]”, or “ârté-fû-iàn”. A contraction exists, so we'll use that “âre-fû\\ià/”. Note the tones.

Now, let's dive into the verb system. We begin by adding the verb morpheme, “-asié-”. Then we add the “to do” and “habitual” morphemes, “pase” and ”âis" respectively. Then we add the subjects and recipients, to do “person-first” and “person-second”, so we get “is” and “tu” respectively.

We mix them all-together and get “âre-fû\\ià/-asié-pase-âis-is-tu”, literally “(love)-\[verb\]-\[to do\]-\[habitual tense\]-\[first person\]-\[second person\]”. We can put the sentence together and get “âre-fû\\ià/asiépaseâisistu”. It's a very long word.

Since verbs are subjective and rely on reader and speaker interpretation, there is no intransivity (forcing a verb not to have an object). This means that there is a lot of freedom in verbs. English “kill” and “die” are two different verbs forcing either having or not having a subject, which is basically killing the essence of verbs. Take, for example, “rasié fût is tu”. This means, literally “\[sleep\]-\[future tense\]-\[subject i\]-\[object you\]”, so it means “i will make you sleep”, which is a threat. Then, with the same verb, we can do “rasié fût is”, which is like the above but with no “you”, so it means “i will sleep”.

### Optimising

We've already optimised with the contraction, but there's more we can do. A lot of stuff can be inferred from the usage of the slot system. For example, the verb morpheme, to do morpheme, present tense morpheme, and first person morpheme are all optional. This means we can shrink our sentence down to “âre-fû\\ià/-âis-tu-o”, by adding an o to the second person to specify them as the object.

As we can see, we have managed to lose a lot of morphemes very easily. These contractions and defaults are what makes the language bearable. 

### An overview of structure

What we have just done is built a sentence. To recap:

> \[concept\]-\[explainers\]-\[people doing things\]-\[some extra particles\]

The sentence we built was in VSO order, where the noun root was turned into a verb. VSO is not the only way to structure a sentence; you can have all of the ways.

### More advanced structure

Let's look at cases, adjectives, and more free structures.

We can add a case-particle to a sentence and make the things inside more free. We should now free ourselves from thinking in terms of slots, but instead in terms of “concepts”. 

Each concept is formed of a noun-root and modifiers. Multiple noun roots can be put together into the same concept via “combinator morphemes”, which combine nouns into one. You can then add modifiers to add connotations and other cool info. Let's make an adjective, for the feeling we get from air. First, we start with the breezy kind of air, to get "đouđou". Then, we add the adjective and feeling morphemes, to get “đouđou-dell-fêll”. This adjective, now fully developed, can be used to describe a noun by adding a little ending stating where it belongs, such as “danes” or “daf”

For example, if instead we wanted an OSV version of "âre-fû\\ià/-âis-tu-o””, we first split our thing into the consituent parts:

*   âre-fû\\ià/-âis - Habitual verb
*   is - Subject
*   tu - Object

Let's first explain cases before readjusting. Cases allow us to tell what role a concept plays. They always go at the end of their respective concept, and allow the brain to readjust for a new concept to arrive.

So, now, we can add a case to both the subject and the object and put them together to get:

> tu-o-is-è-âre-fû\\ià/-âis

The o gives the object case and the è the subject case. Note how they're both at the end.

Let's also look at the isolating version of the VSO sentence. In the isolating structure, we do not get to use shortenings like “âre-fû\\ià/-âis”, instead explicitly having to define every morpheme.

It is quite simple to convert; find the synthetic version of the morpheme with the correct meaning, and then convert that to the isolating one.

> ârte fûr zàn, alâiz tû o.

We can see how every morpheme is now a seperate word, and that the sentence is a lot harsher and less flowing. Commas can be used to seperate concepts.

Also, what we said about “VSO” is false; put what's important first. VSO is good for begginers (and is also the only order that does not need cases), but the whole reason for the case system is to allow other word orders.

For example, take these three sentences (each meaning “i will make you sleep”):

*   rasié fût is tu
*   tu o rasié fût is é
*   is é rasié fût tu o

In each of these, a different thing is stressed. In the first, the act of sleeping is the most important. In the second, the “you” is the most important, and in the first, the “i”.

More nuanced usage
------------------

### Writing a poem

Let's make a poem with the isolating script. We'll use some special features, like individual morphemes.

Why not a simplistic one along these lines?

> Love.
> 
> Does it exist?
> 
> Not with humans for me!

We will begin by translating love into it's modulang equivalent, “ârte fûr zàn”.

Now, let's see about the next one. It's a question, so we'll be adding “ma” at the end. We can phrase it simply as “\[pronoun of previous mentioned object\]-\[thing\]-\[adjective\]-\[to exist\]-\[adjective refers to previous noun\]-\[question\]”

This becomes “eur d'es exle d'antés ma?” (note for speakers of languages which use “d'” as a contraction: Remember that the apostrophe is a glottal stop, not a contraction. It's pronounced “d-es”, with a hard attack on the second syllable, if you will, and not “dës”, where the diersis seperates syllables).

And then we can finally translate the last sentence to make "\[pronoun for second-previous noun\]-\[with\]-\[people\]-\[adjective\]-\[point of view\]-\[first person\]-\[negative\]-\[exclamation\]”

This becomes “tođ lô zàn dell exle fûr ist ne, pak!”

So, a translation would be:

> ârte fûr zàn…
> 
> eur d'es exle d'antés ma?
> 
> tođ lô zàn dell exle fûr ist ne, pak!

Since this is a poem, we can also decorate it with tones with no ill-effects.

> ârte fûr zàn…\\
> 
> eur d'es exle d'antés ma?/
> 
> tođ lô zàn\\ dell exle fûr/ ist ne-, pak\\!

### Insults

No language is fun without insults. Luckily, modulang has a series of modifier to express insult. You'll be seeing the letter ħ a lot here.

The first thing we'll look at is the “bad” morpheme for adjectives, “ixħre”. This can be used to express that something is bad. You might say “đox d'es ixħre” (synthetic: đor-dell-ikré) to express a bad thorn. 

You might use the negative connotation morpheme for expressing something is negative, like “mokû-âis-mû-è-iàn-o-ħ'a”  (“the animal eats people”, or "\[food\]-\_\[verb\]\_-\[habitual\]-\[animal\]-\[people\]-\[negative connotation\]"), where “ħ'a” is used as a morpheme to say that the whole thing is just… bad. 

That's not it. You can say that people are bad, by doing “\[thing\]-\_\[verb\]-\[present\]\_-\[bad\]-\[second person\]”, or “pá-ikré-tu”. Then at the end you, can choose any of four (yes, four to choose from!) disrespect modifiers:

|     |     |     |     |
| --- | --- | --- | --- |
| **Meaning** | **Anyltical morpheme** | **Synthetic morpheme** | **Notes** |
| Friendly disrespect | đut | đllut | Gentle banter disrespect |
| disrespect | ħ’ħuk | \-đuit- | (sounds like spitting) for disrespect anybody |
| absolute disrespect | ħit | \-ħrít- | Only for serious disrespect to what you hate |
| THE WORST DISRESPECT | ħ’ħ’llđllđ | ħ’llđllđ | Extremely offensive; never use except with the very worst |

I'd like to warn you not to use the last two if possible; the first two are plenty for those you generally dislike.

If you REALLY hate the person and want them to be tortured for eternity in hell, you might want to use the last morpheme (if you can say it).

Since this is a conlang, i kindly request you follow this rule and keep this a horrific swear word for the times it becomes important.

Comparisons
-----------

Modulang is an intresting language, similar but not equal to other languages.

*   **Mandarin Chinese** - Whilst modulang has tones and a isolating system, it uses far less tones, has no homophones, and only really has words for concrete nouns.
*   **Esperanto** - Modulang might have similar affix-based word construction, but it allows far more flexibility in sentence order. Modulang has less abstract nouns and is much smaller.
*   **Toki Pona** - Modulang has a larger volcabulary, richer system of affixes, free-er word order, but a similar number system and a nice isolating system.