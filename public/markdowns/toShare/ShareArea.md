# ShareArea
Introduction
------------

ShareArea is the place for mods created with this tool to be shared. It bears a strong resemblence to Scratch's area for sharing, but takes most project management features from Git. 

Browsing Area
-------------

The browsing area, by default, is the place for finding project. It features several “banks” of projects, which are little collections based off metrics, bearing a similarity to ContentDB's and Scratch's system.

This place is optimised for engagement and discovery, ensuring an even spread across quality projects.

Project Page
------------

There is a project page for each project.

Each project page acts differently for the type of project:

| Type | Acting | Possibilities |
| --- | --- | --- |
| Mod | Shows the contents of a mod and their activities | Allows editing |
| Modpack | Shows the component mods and allows individual editing, as well as viewing “ghost things” from other mods |
| Game | Shows the component mods and modpacks, allowing editing of all things but also focus onto individual mods and modpacks |

Alongside this, there are tools available for project managment, not too disimilar from a real git hosting site.

**Non-owner views**

Comments are issues; people can choose if their comment is a “comment” or “request” or “issue”. Contributors can change them.

Each project allows viewing the mod, and any change automatically “forks” the project onto the user's userspace. This allows them to work indepenedently.

Each fork can then be “pull-together” requested back into the original, which allows new changes to come.

**Owner views**

The owner themselves sees some new functionalities. They have several levels of "done” they can add to a comment, and can also merge forks onto the project (but only after a pull-together request has been made).

Whenever they upload to a project, they make a new "commit" to the project, alongside a message of what has changed and further optional description.

They can also organise their project into “versions”. This is an almost-identical copy of Git's tagging system.

Similarities
------------

As we have seen above, there are similarities to common platforms:

*   Scratch
    *   A place to “remix” and “see inside” projects
    *   Browsing similarities
    *   Overall user interface similarities
*   ContentDB
    *   Browsing
    *   Comments
    *   “Project approval” and “comment approval” systems
*   Git repos
    *   Project management powers
    *   Backwards-compatible with standard git commands (but in an opauqe way)

ContentDB integration
---------------------

For higher quality mods made with this project, ContentDB may be integrated. This means that every commit and version, the ContentDB will updated accordingly.

The backend takes care of Lua conversion and publishment with a “proxy git repository” for the Lua code only.

When a developer wishes to integrate from the ShareArea, they must first submit an “official request” to the admins, who will review the mod before it is sent over.

This approval procedure ensures that no “crappy" mods enter ContentDB. This important step prevents ContentDB from having to enforce a ban on ShareArea mods.

Anything from ContentDB will be given to the individual project owners, making sure they don't have to leave the ShareArea.

Should a project owner wish to leave ShareArea in lieu of ContentDB, this should be fully allowed. The ShareArea MUST make this transition as easy as possible.

This transition is done by the following procedures:

*   Allowance to clone the git repositories needed
*   Control over the ContentDB repository
*   Removal from ShareArea alongside a JSON export of what was there

Hosting
-------

ShareArea follows the same hositng approach as ContentDB; be centralised and hosted on a single server, but also allow others to host their own versions.

The entire thing can come as a docker container hosted on the docker marketplace. This makes it almost trivial to install and run and allows private hosting.

The benifits of self-hosting such a niche product are not know, but might be good for environment where a large group of people need management of their own mods whilst also needing the security of a closed system.

Project roadmap
---------------

This project should ONLY be pursued AFTER the main creator is mostly prepared for client usage. If this is not done, then this whole project might be a waste of time.