# New notation system
Background
----------

Historically, the current lyrical notation has been sufficent for most songs in existence; most songs were either live or came from live recordings and had a main singer and some other singers in the background sometimes.

However, with the rise of electronic music, new methods of modifying lyrics have been released and are unaccounted for in the current scheme, despite their low ability to accurately transcribe how the lyrics are made.

There are some goals of this system:

*   Reflect vocals more accurately
*   Provide instruments when needed
*   Reflect pitch changes and give a more accurate way of writing.

Features
--------

| Indicator | Meaning | Example |
| --- | --- | --- |
| ^   | Indication of song data with these accepted values:<br><br>*   `title`<br>*   `artist`<br>*   `year`<br>*   `album`<br>*   `length`<br>*   `producer`<br>*   `data` (any of the below; applied to whole song and all other modifiers are relative to the data) | ^title: song ^artist: her ^year:2023 |
| /   | Indication of “higher” or “up” |     |
| \\  | Indication of “lower” or “down” | Less-important verse: \[verse 1 \\\] |
| \-  | Indication of “normal” | Normal voice: \[\[-\]\] |
| \*  | A multiplier to indicate quantity of something | Two repeats: \[chorus\*2\] |
| +   | A multiplier to indicate mass of something | Louder: \[chorus+1\] |
| (seperated by spaces from encasing) \[ \] or / / | Indicates an IPA-transcription | Non-English sound in an English song: ( /βɔ/ ) |
| ( ) to (((( )))) | Indication of how “ghosted” lyrics become, more brakcets meaning more ghosting | Backing: (ooh /)  <br>Barely audible whisper: (((( \[\[\[whispered\]\]\] you won't be able to )))) |
| \[ \] | Indicates a part | \[Chorus\] |
| \[\[ \]\] | Indicates a new singer or pitch (with the vertical marks, indicates pitched vocals) | Vococoding with two seperate lower pitches: \[\[/ - \\\*2\]\]  <br>Vocoding with doubly-loud lower pitch: \[\[/ - \\+2\]\]  <br>Singer: \[\[ John Smith\]\] |
| \[\[\[ \]\]\] | Indicates form of voicing, notable instrumental feature, or reasoning for feature (for interpretation) | Voicing: \[\[\[breathless\]\]\]  <br>Instrument: \[\[\[mocking synth\]\]\] |
| { } | Indicates non-transcribable features. | Sound effects, pitched down: {heavy breathing \\} |
| < > | A change in the music | Key change (up): <c# maj />  <br>BPM change: <125 bpm \\ >  <br>All change: <c min \\ 90 bpm /> |
| > < | (appended) Indicates scope of a change, if change is not indicated by a marking | \[\[\[screaming\]\]\] > Until verse 3 repeats < |
| xx: or xxx: | Indicates a change of language given an ISO language code | Change to Spanish: es:  <br>Change to Toki Pona: tok: |
| #   | Comment (at end of line) | Meaning: And here i stand # This line mainly refers to standing alone<br><br>Musical element: You're the worst! # The music swells and “breaks” apart |

Extension Scheme
----------------

Since the amount of voice effects are increasing, diversions from the above abilities are permitted, as long as they follow these rules:

*   Any markers must be fully usable using only the ASCII charset. Where characters are not available using ASCII or have been substituted, the substitutions must be documented. (just to be clear, this does not include content, so, for example, the IPA transcriptions inside / / or \[ \] are not an exception)
*   If the markers already exist, use those instead
*   A definition must be placed above the lyrics (example: “% % - Indicates half-sung lyrics”)
*   Any markers made must not make standard markers more poorly defined

Example lyrics
--------------

### Brokeup, Arca

This song uses many voice-pitch effects, which allows it to be a powerful demonstration of the system's pitch notation.

```text-plain
((Uh, uh))

[[-]]
swear, man, get off
Waitin' for my flight to take off
We're gonna faint and break up
Bloodless and gettin' paper
Uhuh, 

[[/]]
I'll push and pull it out, breed it, more
I'll push and pull it out
It's too much for me to take
It's too much for me to take
I'll push and pull it out, wait
It's too much for me to take
It's too much for me to take, ok?

[[-]]
Workin' thick on knees to push it on heels
I hear, throat sore, resting on your arms
Smeared makeup, waiting for breath take off
Feeling faded, break out, blood rush, paper
I'm a little bit twisted
Reel it back then feel it, seal it
Arms up
I'm a little bit twisted
Reel it back then feel it, seal it
Hold up
Hey, okay, I'll wait
Uh, wait
I don't feel my body leaning
Worst comes to worst
I'm feeling like a natural heathen
Superstition, blood, I'm paused
Wait for you

[[/]]
I'll push it
It's too much for me to take
It's too \ much for me to take
So, where you goin'?

[[/]]
Push it on knees, push it on heels
I hear, throat sore, resting on your arms
Swear, man, get off
Waitin' for my flight to take off
We're gonna faint and break up
Bloodless and gettin' paper
I'll push and pull it out, breed it
It's too much for me to take
It's too much for me to take
It's too much
```

Oh My God, Sevdaliza
--------------------

```text-plain
[[/+2]]
Oh my God
Who should I be? (([[[chopped-off]]] ee-aah))
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

[[/]]
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

I've been through a lot in life
I live up
Distant from it all
I view myself from above
Roamin' in the fields of hope
Will it make or break me?
As my dreams are heavy, they outweigh me (([[[echoey]]] ko, ru: Ichima, спасибо))

Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

True that you're not alone
True that you're not alone
(Will it make or break me?)
True that you're not alone
True that you're not alone

O-o-oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

[[[repeated, echoey, choral]] en:
True that you're not alone

[[[echoey]]] ko, ru:
Ichima, спасибо

[[[repeated, echoey, choral]] en:
True that you're not alone

Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

(True that you're not alone)

[[/]] [[[frequently cut-off]]]
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell
```