# FOSS Funding Company (FFC)
Introduction
------------

Historically, the FOSS community and software body has been underfunded and mainly relied on passion and love. This has resulted in an inoptimal development speed and the dominance of propietary software.

Money is the universal unit of appreciation and quality of services, and it is also the gateway to better software and dominance over the Propietary software.

As such, i propose this company to distribute funds.

Goal
----

The goal of this company is to provide a single body for EVERYONE to be able to donate to. This means that FOSS donations are simplified. The company also gives FOSS developers a fair payment.

Repository guidelines
---------------------

Repositories, to be eligable to this program must:

*   Already have a team of at least 5 different contributors having sent pull requests in the past month
*   Have a ten-point “quality-level” tag system judging the quality of pull requests and issues based on:
    *   How innovative they are
    *   How stable they are
    *   How important they are
*   Send a “opt-in” letter to the program, possibly on request

Once done, they can then opt-in and be approved by community vote with admin veto if neccary.

Opting in will give several benifits:

*   A weekly income based off contributor amounts (allowing much easier progress)

Basic workflow
--------------

A contributor will use the website's repository finder to find a repository they enjoy, based off a tag system which lets them sort based on:

*   Main languages used
*   Type
*   Main technologies used
*   Average contribution score from the past 7 days (which means that developers can target poorly-moving repositories for more money)

From there, they can then go to the repository and either add issues or pull requests based off issues. Both are ranked by the development team using the tag system. However, they are only calculated and counted as part of the payout if they're merged.

### Introduction to intrested parties

**Donors** are those who give money to the charity. This then gives them benifits as well as gives the FOSS world the money it desperately needs

**Repositories** Are the FOSS software which opt-in to recieve money from the charity. They are encouraged to gain developers and a  higher average feature score if they want more money. This money can be spent as the developers wish; they may use it for servers or themselves

**Contributors** Are those that contribute to a repository. They recieve most of the money for their changes

Cash Flow
---------

### Basics

The company is a non-profit charity. It takes money from donators and distributes it among contributors and repositories based on the value of a pull request to 

### Financing

The corporation is funded by donators and sponsors, who give money to FOSS. The incentive is that funding it will have a result in your life. Any person who uses FOSS software but has no development skill may pay the company; this results in effects directly available to them.

Since this is a single body which you donate to, the logistics of donations are much easier for the payer.

Here are reasons people may wish to donate:

*   Make FOSS software competitive against propietary, which benifits everyone as other individuals do not want to pay for propietary software when FOSS can do the job  million times beet
*   A very easy way to give back; no time needs to be spent, just a little of your wage (which i guess is an abstracted form of time)

Paying also gives some “convinience features”, starting at €1 per week:

*   A single place to sync software and versions across devices
*   A place to find novel and intresting software
*   Access to the monthly newsletter, which adds to the newsletter about the changes and the finances, giving payers a better idea of what's happening

And at €5 per week:

*   A little badge next to their name on the website should they be in the top 50
*   Stickers and other memorabilia
*   A single place to get all alpha builds, built hourly (bleeding edge!)
*   Voting access for various policies
*   Choice of fund direction

The fund direction is very helpful; they can channel 50% of their donations to a specific repository.

### Contributors

Contributors get, by default on their first pull request, and until their latest pull request is a week old:

*   Stickers and other memorabilia
*   Voting access for various policies

To figure out the money given to a contributor per week, we use the dummy value of €1.

From here, we then use a series of mutations for each contribution:

1.  Per contribution
    1.  The amount has 10% removed if it's an issue, which should incentivise pull requests (which are what moves the development forwards most directly).
    2.  The average ranking per issue or pull request (as applicable) is then calculated.
    3.  Each point that the contribution has above the average, 10% is added to the payout, which incetivises work on poorly-commited repositories the most. If the contribution is less, nothing is removed.
    4.  1% of the remaining money is then given out to the repository (as a small payment for the code review and feature ranking (which is incentivised to be wide due to the “above the average” clause)), with the rest going to the contributor.

So, for example, if a developer made a level-7 pull request on a repository with an average score of 6, then they earn €1.50 (€1.40 after repository payout), but if they did the same  on a place with a average score of 2, they earn as much as €3.50 (€3.40 after repository payout).

The total payout is then calculated finding these rules and then adjusted to the actual budget, with each contribution's earnings being given the correct transformation.

If a developer submits enough poor pull requests to push a repository down by 0.1 feature points on average, they have the “repository tax” increased by 1%.

### Repositories

Each repository gets a set amount of money based on money given to contributors; it's 1% of this money, incentivising good contributors. This also forces repositories to promote themselves and get all the good contributors.

Low average feature scores results in low money amounts. This should incentivise them to get high-quality contributions.

Every week, just before payout, a repository will be checked to ensure feature rankings are fair. Being unfair may lead to being removed from the program.

Goals
-----

### Intended incentives

This charity should incentivise:

*   Repositories to sign up to recieve money.
*   Contributors contributing to the worst repositories the most
*   As many contributions as possible
*   Both pull requests and issues
*   Merging and ranking of pull requests
*   Donations to help improve FOSS as a whole

### Job replacement

Should the charity earn enough money, it might be a viable alternative to jobs. Should the funding be enough, contributors may then flock to the program.

This may lead to a “tragedy of the commons” kind of situation, where more contributors working makes all the contirbutors lose money. As such, contributor enrollment is kept under control to keep wage at a sensible level. Possibly, to avoid this, part of the enrollment process may be asking for at least €50 donated per week in total 

Advert
------

### To clients

#### In general

Looking for a way to give back to the community and make a positive impact on the world? Consider donating to the world of Free and Open-Source Software (FOSS)!

FOSS is a global movement that promotes the use and development of software that is freely available for anyone to use, modify, and distribute. This software is created by a community of developers who work together to create innovative, powerful tools that can be used for everything from simple word processing to advanced machine learning and data analysis.

By supporting FOSS, you're helping to ensure that everyone has access to the tools they need to succeed, regardless of their financial means. This includes small businesses, non-profit organizations, and individuals who may not have the resources to purchase expensive proprietary software.

But FOSS isn't just about providing access to software - it's also about building a more open and collaborative world. FOSS developers work together to share knowledge, exchange ideas, and create solutions to some of the world's biggest problems. From healthcare to education, FOSS is transforming the way we work and live.

So why donate to FOSS? By supporting this important movement, you're helping to:

*   Promote open access to technology and knowledge
*   Foster innovation and creativity
*   Empower individuals and organizations to achieve their goals
*   Build a more collaborative and equitable world

Your donation, no matter how small, can make a big difference in the world of FOSS. By contributing to this movement, you're helping to create a better future for us all. So please, consider donating today and join us in the fight for a more open and collaborative world!

#### Other methods

Other methods of promotion are also given:

*   Supermarket donation buckets, where people donate to FOSS
*   Online “donate” buttons funding this charity, meaning users don't have to pick and choose their favourite developers.

### To Repositories

Are you a part of a FOSS repository that's been struggling to get the support it needs? We're here to help!

Our charity organization is dedicated to supporting FOSS communities and the repositories that they rely on. By joining our program, you'll have access to a neat stream of funding that can help your repository thrive.

We have a simple opt-in process that only requires a letter of interest from your team. Once you're a part of the program, you'll be able to benefit from our unique contribution ranking system that incentivizes developers to work on repositories with the most potential for improvement.

But that's not all - joining our program also means that your repository will have a greater presence in the FOSS community. Our program promotes the best and most innovative repositories, giving your team the recognition it deserves.

Don't let your repository be left behind. Join our program today and watch your project flourish!

### To Contributors

Attention all developers and open-source enthusiasts!

Are you tired of contributing to projects that are underfunded and underappreciated? Do you want to make a real difference in the world of open-source software?

Look no further than our non-profit charity for open-source development. By joining our program, you can receive funding for your contributions to open-source repositories, and help push the world of FOSS forward.

We offer a unique system of payouts based on the quality of your pull requests and issues, incentivizing contributors to work on the worst repositories the most. And with our 10-point quality-level tag system, you can rest assured that your contributions will be evaluated fairly and transparently.

Not only will you be making a difference in the world of FOSS, but you'll also be joining a community of like-minded individuals who share your passion for open-source development. And with the potential for job replacement, you could even turn your passion for open-source into a full-time career.

Join us today and start making a difference in the world of open-source software!