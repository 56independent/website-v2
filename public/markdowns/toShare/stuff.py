import sys

def make_text_work(text):
    return text.replace("\\", "&#x60;").replace("`", "¿¿putabacktickhere")

def export_file(name):
    filename = os.path.splitext(file)[0]

    # Extract the contents of the file
    with open(file, "r") as f:
        contents = f.read()

    # Create a dictionary with the extracted information

    # Append the dictionary to the output file with pretty-printing
    with open(output_file, "a") as f:
        f.write("""{
        "name":'""" + filename + """',
        "description": "",
        "tags": [""],
        "content":`\n""" + make_text_work(contents) # Some fucking escape crap for my shitty code fuck this
        + "\n`},")

def export_all():
    import os
    import json

    output_file = "output.json"

    # Remove the output file if it already exists
    if os.path.exists(output_file):
        os.remove(output_file)

    # Iterate over each markdown file in the directory
    for file in os.listdir("."):
        if file.endswith(".md"):
            # Extract the filename excluding extension
            export_file(file)

if len(sys.argv) == 1:
    export_all()
else:
    argument = sys.argv[1]
    if argument.endswith(".md"):
        export_file(argument)
    else:
        print(make_text_work(argument))