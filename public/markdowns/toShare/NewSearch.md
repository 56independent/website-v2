# NewSearch
Introduction
------------

Historically, search engines such as Google and DuckDuckGo have been centred around using text to basically `ctrl`+`f` across the internet.

Whilst this approach works extremely well for quickly finding answers and solutions, it leaves most of the internet obscure. Ever heard of the bucket ring? Euphoria.io? Anything published by “D!NG”?

Search engines are horrible at this. What about a “website index”, where people can submit their websites and have them listed?

User views
----------

Each type of user sees a different thing. Mostly, it's a bit like Reddit, but a TRUE homepage of the internet.

### Members

Members form the majority of the recorded population. They have an account, with which they may submit comments and “upvotes” and “downvotes”.

Each member can “subscribe” to tags and see new things.

The website is organised into pages based off tags mixing; for example, “shopping” may be a tag, and then we might have “computing” as another, and maybe later “adobe”, to show where we can buy adobe products.

### Non-members

Non-members can do most of the same stuff as members, but from a read-only perspective. They cannot upvote or downvote, subscribe to tags, or leave comments.

### Website-contributors

Webmasters and non-associates of websites may submit websites for review. They must first be a memeber. They write a description, some tags, and send it for approval.

Webmasters may opt-out of hotlinking.

### Admins

Admins are those in control of managing all websites and tags. They can easily create and remove tags, alongside bulk actions on websites. For example, they can rename tags and split or merge tags.

### Tags

Tags form the bread and butter of the system. Every tag categorises websites and people can add or remove them from their search. 

The tag system might categorise ContentDB with these tags:

*   Associated websites (only websites with a direct link)
    *   minetest.net
    *   rubenwardy.com
*   Content
    *   mods
    *   games
    *   minetest
    *   community
*   Features
    *   likes
    *   dislikes
    *   comments
    *   community-uploads
    *   open-source
*   Technology
    *   bootstrap
    *   sql
    *   https

And Youtube with these:

*   Associated websites (only websites with a direct link)
    *   google.com
*   Content
    *   community
    *   videos
    *   music
    *   gaming
    *   algorithm-based-working
*   Features
    *   likes
    *   comments
    *   community-uploads
    *   closed-source
*   Technology
    *   ???

Client-server relationship
--------------------------

The server holds ALL the data, from the tags, websites, descriptions, and comments.

When a client requests data from the server, it will also end up with:

*   The HTML template to work on
*   Javascript code
*   Some data:
    *   The entire list of tags (could be bad; later optimisation needed)
    *   A collection of objects relating to the websites and their information

Pages
-----

These are the main pages, ordered by time i will finish programming them:

*   `tags.html`
    *   Is a directory of tags
    *   Shows us the tags, their descriptions, and how many websites have them.
    *   Lets us sort
    *   Server finds the tags and their descriptions, sending them to the client
*   `search.html` (with the `?search=` argument)
    *   Shows us websites matching those tags
    *   Server-retrieved, client-sorted and presented
*   `index.html`
    *   Introduces the website
    *   Tells us about the stuff
    *   Lets us do some browsing (by transcluding browse.html)
*   `browse.html`
    *   Shows us some curated websites, based on metrics:
        *   100 most used websites
        *   100 most upvoted websites
        *   5 randomly-picked >10 upvote websites
*   `tagManager.html`
    *   Is a place for admins to perform bulk tag actions
    *   Allows creation and removal
*   `websiteManager.html`
    *   Is a place for admins to perform bulk website actions
    *   Allows retagging, creaiton, and removal.
*   `user.html`
    *   Is a login screen
    *   Used to access the website as a member

Database
--------

The website uses SQLite to hold data. This will be my first time with databases (!!) so wish me luck.

Here are the data storage devices:

*   Websites
    *   SQLite
    *   Stores:
        *   Website names
        *   Website descriptions
        *   Websites' tags
*   Tags
    *   JSON
    *   Stores:
        *   The tag name
        *   The tag description
        *   The tag category
*   Users
    *   SQLite
    *   Stores:
        *   Usernames
        *   Passwords
        *   Permissions
        *   Other user-specific data
*   Community
    *   SQLite
    *   Stores:
        *   On a website-basis
            *   Comments
            *   Upvotes and Downvotes

There is some duplication between community and website databases. To help clarify what i mean:

The **Website Database** is for storing information on Websites; it should be a standalone portal to the rest of the web.

The **Community Database** is for storing information on User Contributions. It acts as a seperate checker and verifier of the community.

Contributors of websites
------------------------

Users are always the first contributor of websites; their hard work ensures that websites are documented in the first place.

However, alongside the user, there is also a series of bots which “crawl” across the web. They look for new domains and try to scrape for info relating to them.

Let's see how a bot might find `forums.minetest.net`, for example:

*   Discovers main page
*   Notes down PHPBB as a technology tag
*   Notes down the description
*   Finds subdomains

And then this is added to the queue to be managed later

Backups
-------

The server will automatically backup all databases into a zip archive, with these frequencies:

*   Yearly
    *   Never deleted
    *   Happenes every date who's month component is 01 or 06, with a 00 day component.
*   Monthly
    *   Deleted after 6 months
    *   Happens every day which has day component 00
*   Weekly
    *   Deleted after 3 months
    *   Happens every date of which the day component modulus by 7 equals 0 happens 
*   Daily
    *   Deleted after 30 days
    *   Happens at 00:00
*   Hourly
    *   Deleted after 24 hours
    *   Happens at minute-mark 00

The backups overlap; the names have a timestamp to allow us to automaticalpy remove things that happen.

The backup library is a seperate library owing to its utility. It is configurable.

A backup is made every hour; the way backups with larger frequencies are made are by not deleting them until they get too old.

Differences from existing engines
---------------------------------

There have been previous categorisation attempts. Each of these have failed or are closed to the public; mine indexes the web in a very public manner with a very large tag system. 

My system also differs from traditional search engine; little focus is on finding the content and searching through that, but more rather in categorising websites and what they're FOR. This engine is horrible for finding solutions to that error on StackOverflow, but it's great for finding unique novel websites.

The main data stored is what the websites contain and what they're for. 

### Other

The websites are shown as “preview” iframes. Failing that, screenshots are used. 

Iframes will only ever be loaded if neccesary.

The entire thing is under MIT license, including the website database. The user credentials and contributions are mostly private.

The website instance i own does not allow any website mainly used for the following content:

*   Porn
*   Gambling
*   Hard alcohol
*   Terrorist organisations
*   Drugs
*   Hate speech
*   Graphic content
*   Illegal activities
*   Instructions for illegal activities
*   Pirated or copyrighted material
*   Phishing or scamming activities
*   Malware
*   Self-spreading programs
*   Spam or unsolicited advertising
*   Personal or sensitive information without consent
*   Harassment or cyberbullying
*   Child exploitation or explicit material involving minors
*   False or misleading information
*   Political propaganda or extremist ideologies
*   Offensive or abusive language
*   Animal cruelty or unethical treatment of animals
*   Invasive surveillance or privacy violations
*   Counterfeit goods or replica items
*   Witchcraft or occult practices
*   Nudity or explicit content
*   Weapons, firearms, or ammunition
*   Hacking or unauthorized access to systems
*   Illegal substances or drug paraphernalia
*   Fraudulent activities or scams
*   Ponzi schemes or pyramid schemes
*   Virus hoaxes or false information about diseases
*   Stalking or harassment of individuals
*   Plagiarized or stolen content
*   Unauthorized sharing of personal information
*   Copyright infringement or intellectual property violations
*   Unauthorized streaming or distribution of copyrighted material
*   Prostitution or solicitation of sexual services
*   Racist or xenophobic content
*   Cults or extremist religious groups
*   Bullying or cyberbullying
*   Phobias or triggering content without appropriate warnings
*   Graphic or disturbing medical procedures
*   Dangerous or harmful activities that could result in bodily harm
*   Advocacy for self-harm or suicide
*   Defamatory or libelous statements about individuals or companies
*   Encouragement of illegal activities or illegal behavior
*   Graphic violence or gore
*   Animal fighting or cruelty
*   Scatology or explicit discussions of bodily functions
*   Voyeurism or non-consensual sharing of intimate content
*   Discrimination based on race, ethnicity, religion, gender, or sexual orientation
*   Violation of privacy laws or regulations.
*   Zombie-related content that may induce panic or incite zombie apocalypse fears
*   Promotion of time travel or interdimensional portal devices without proper authorization from relevant authorities
*   Content involving sentient vegetables or fruits engaging in illicit activities
*   Unapproved recipes for potions, spells, or magical concoctions
*   Discussions or tutorials on building functional yet illegal robots or cyborgs
*   Historical revisionism or denial of well-documented events, such as the moon landing or the existence of dinosaurs
*   Promotion of intergalactic colonization or extraterrestrial immigration without proper interstellar visas
*   Unsubstantiated conspiracy theories involving shape-shifting reptilian overlords or sentient household appliances
*   Seditious content encouraging rebellions against fictional or non-fictional governments
*   Guides on how to communicate with imaginary friends, including tips for ghost summoning or telepathic communication with unicorns
*   Unlicensed fortune-telling services or psychic predictions that guarantee world domination or eternal youth
*   Content glorifying the use of superpowers or supernatural abilities for personal gain or global domination
*   Promotion of secret societies or exclusive clubs that require initiation rituals involving exotic animals or mythical creatures
*   Unauthorized exploration of parallel dimensions or alternate timelines without proper interdimensional travel permits
*   Discussions on conspiracy theories involving alien encounters at fast-food drive-thrus or encounters with time-traveling Elvis impersonators
*   Promotion of underwater cities or habitats for mermaids and mermen without proper environmental impact studies or aquatic zoning permits
*   Guides on constructing fully functional pirate ships or airships without proper maritime or aviation certifications
*   Unverified claims of possessing the philosopher's stone or other mythical objects with supernatural powers
*   Promotion of forbidden love affairs with vampires, werewolves, or other supernatural creatures without consent from their respective supernatural councils
*   Content involving telekinetic or mind control techniques for purposes of pranks or mischief without considering potential ethical implications or psychological damage.
*   Unapproved interplanetary colonization plans or guidelines for establishing civilizations on celestial bodies
*   Content encouraging the creation of time loops or disruptions in the space-time continuum for personal amusement
*   Unauthorized promotion of mind-reading technologies or telepathic communication devices
*   Guides on building functional, unregulated teleportation devices or wormhole generators
*   Discussions or demonstrations of summoning legendary creatures, such as dragons or phoenixes, without proper permits from mythical creature conservation agencies
*   Promotion of illegal intergalactic smuggling operations involving exotic alien species or rare extraterrestrial artifacts
*   Unverified claims of possessing the Holy Grail, the Ark of the Covenant, or other mythical religious relics
*   Content advocating for the use of mind-control techniques on elected officials or world leaders
*   Unauthorized development or distribution of advanced artificial intelligence systems capable of overthrowing governments or initiating global dominance
*   Discussions or tutorials on creating permanent invisibility cloaks without proper consent from the International Association of Illusionists and Tricksters
*   Promotion of time-travel tourism packages without ensuring adherence to historical preservation laws or the prevention of temporal paradoxes
*   Unsubstantiated theories or guides on communicating with extraterrestrial civilizations without proper interstellar communication licenses
*   Content encouraging the establishment of sovereign nations in unclaimed territories, such as underwater or on unexplored islands
*   Unauthorized promotion or sale of elixirs or potions promising eternal youth or immortality
*   Guides on summoning interdimensional beings for casual conversations or entertainment purposes
*   Content involving conspiracy theories about ancient civilizations being ruled by time-traveling dinosaurs or sentient robots
*   Unapproved content promoting mind-transfer technologies or consciousness uploading without ensuring ethical considerations and consent
*   Discussions or guides on building functional space elevators without proper certifications from celestial infrastructure regulatory bodies
*   Promotion of intergalactic sports tournaments or betting on alien competitions without valid interstellar gambling licenses
*   Content glorifying the use of mythical artifacts or mystical relics for personal gain or world domination
*   Unverified claims of possessing the Fountain of Youth, the Philosopher's Stone, or other legendary sources of eternal life
*   Unauthorized exploration or settlement of parallel dimensions inhabited by fictional characters or creatures from folklore
*   Content advocating for the establishment of utopian societies based on unconventional principles, such as gravity-defying cities or cities made entirely of chocolate
*   Discussions on conspiracy theories involving time-traveling clowns or secret societies of shape-shifting circus performers
*   Unlicensed tutorials on building functional, autonomous robot armies without proper regulation or consideration for global peace
*   Promotion of interdimensional trade routes or smuggling operations involving rare and exotic interdimensional goods
*   Guides on creating functional, unregulated devices for mind control or hypnosis on a global scale
*   Unsubstantiated claims of possessing magical artifacts, such as the Sword in the Stone or Aladdin's Lamp
*   Content advocating for the establishment of interplanetary colonies without proper consideration for sustainable resource management or extraterrestrial environmental impact studies
*   Unauthorized discussions or promotion of time-manipulation techniques, including time loops, time dilation, or temporal anomalies
*   Content encouraging the use of mind-reading or mind-control abilities on unsuspecting individuals for personal entertainment or amusement
*   Guides on constructing interstellar spaceships without valid intergalactic travel licenses or adherence to cosmic traffic regulations
*   Discussions or demonstrations of summoning interdimensional beings for mundane tasks or trivial matters
*   Promotion of forbidden love affairs with mythical creatures or legendary beings without proper consent from their respective mythical creature councils
*   Other “bad” stuff, as defined by me, maybe including:
    *   Twitter
    *   Russian-government-backed websites with little value

However, owing to the open-source nature, these bad things can be circumvented. Some wierdo may put a porn and drugs directory with a side of hard alcohol.