# Chinese encoding system
Currently, the leading encoding system for Chinese is Unicode. This system is not very elegant. Every single character gets a codepoint. There is little regard to the intecracies of Chinese writing. What if we changed that? What if we made an encoding system simpler and more systematic and clearer to understand?

Let's first have a crash-course on Chinese characters.

Chinese Characters are formed of components called **radicals**. There are about 250 of them and they can be put together to form **characters**.

Radicals will often be stretched and strokes within them changed to fit into a character. 

Radicals can be organised within a character in many different ways. 

The character formed by radicals might have its meaning hinted at by the meanings of radicals.

For example, take the character 休. It is formed of two radicals organised left to right; The radical for “person" (人) and the radical for “tree” (木). These two don't directly mean anything, but it kind of looks like the person is leaning on the tree, possibly taking a break. The character 休, therefore, mus mean “rest”.

So, let's devise the encoding system.

The first two numbers always encode the placement of radicals in the character:

|     |     |     |     |
| --- | --- | --- | --- |
| **Number** | **Type** | **Number** | Thing |
| 1   | Directional | 1   | Left to Right |
| 2   | Left to middle to Right |
| 3   | Top to Bottom |
| 4   | Top to middle to Bottom |
| 2   | Surround | 1   | Full |
| 2   | From N |
| 3   | From NW |
| 4   | From W |
| 5   | From SW |
| 6   | From S |
| 7   | From SE |
| 8   | From E |
| 9   | From NE |
| 3   | Overlay | 0   | Overlay |

  
And then afterwards are a series of three-digit codes for each radical in the character as defined by [radicals.csv](Chinese%20encoding%20system/radicals.csv).

After this, optionally, a 6-digit pronunciation guide. The first two digits encode the initial, the second the mid, the third the final, the fourth the final add-on, and the sixth the tone.

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Intial (spread out)** | **Mid** | **Final 1** | **Final 2** | **Tone** |
| ø   | a   | ø   | ø   | none |
| b   | o   | i   | n   | straight |
| f   | i1  | r   | ng  | up  |
| m   | i2  | a   | \-- | down |
| d   | i3  | e   |     | waving |
| t   | e   | o   |     |     |
| h   | u   | \-- |     |     |
| l   | ü   |     |     |     |
| g   | \-- |     |     |     |
| k   |     |     |     |     |
| s   |     |     |     |     |
| zh  |     |     |     |     |
| ch  |     |     |     |     |
| z   |     |     |     |     |
| o   |     |     |     |     |
| r   |     |     |     |     |
| x   |     |     |     |     |
| q   |     |     |     |     |
| w   |     |     |     |     |
| y   |     |     |     |     |
| sh  |     |     |     |     |

The beauty of this encoding system is that it allows any character to be constructed and lets new words with new characters be possible. Since it can also encode pronunciation, it provides a valuable nook for learners to jump from.

With minor changes, this system could also be used to encode other Chinese-based writing systems, such as Kanjii and Hanja.