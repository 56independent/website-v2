function runPlatformer() {
    // width="500" height="300"

    addHelp("<p>Avoid the rocks! Press space to jump. Press r to restart.</p>")

    ctx = prepareCanvas()

    mainColour = "blue"

    x = 500
    y = 150

    jump = false
    goUp = true
    dead = false

    velocity = 1

    if ($("#jumpButton").text().length == 0) {
        $("#controlsArea")
            .append(
                $("<button>")
                    .text("jump")
                    .attr("class", "btn btn-primary")
                    .attr("id", "jumpButton")
                )
    }

    $("#jumpButton").click(() => {
            jump = true
    })

    $(document).on("keypress", (e) => {
        if (e.key === " " || e.key === "Spacebar") {
          jump = true;
        }
    });

    // Restart the game
    $(document).on("keypress", (e) => {
        if (e.key === "r" || e.key === "R") {
          runPlatformer()
        }
    });

    document.activeElement.blur()


    requestAnimationFrame(platformerAnimate);


    function platformerAnimate() {
        const canvas = $("#place")[0]


        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Handle collisions
        if (x <= 120 && x >= 100  && y >= 130 ) {
            mainColour = "red"
            dead = true
        } else {
            x = x-velocity
        }

        // Handle Jumping
        if (jump && !dead) {
            if (y > 100 && goUp) {
                y = y - velocity; // Move upward
            } else if (y < 150) {
                y = y + velocity; // Move downward
                goUp = false
            } else {
                jump = false; // Reset jump when reaching the ground
                goUp = true
            }
        }

        // Our character
        ctx.fillStyle = mainColour
        ctx.fillRect(100, y, 20, 20)

        // The floor
        ctx.fillStyle = "green"
        ctx.fillRect(0, 170, 500, 100)

        // A rock!
        ctx.fillStyle = "grey"
        ctx.fillRect(x, 150, 20, 20)

        if (x < 100) {
            velocity++
            x = 500
            ctx.fillRect(500, 150, 20, 20)
        }

        requestAnimationFrame(platformerAnimate);
    }
}

