// Handles the lobby

lobby(true)

$("#make").click(() => {
    // Become a host and give away the key
    lobby(false)
    becomeHost()
})

$("#go").click(() => {
    key = $("#keyArea").val()

    lobby(false)
    becomeClient(key)
})