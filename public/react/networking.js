
function handleData(data, isServer) {
  var trueData = {}

  console.log(data)
  try {
      trueData = JSON.parse(data)
      console.log(trueData)

      if (isServer) {
          switch (trueData.type) {
              case "change":
                  server.messageQueue.push(JSON.stringify({
                      "type":"changeEvent",
                      "data": trueData.data
                  }))
                  break
              case "new-user":
                  server.messageQueue.push(JSON.stringify({
                      "type":"voteUser",
                      "data": trueData.data
                  }))
                  break
              case "voteAnswer":
                  voteAnswers.push(trueData.data.allowUser)
              default:
                  console.log(trueData.type, " Does not have any rules for handling with the " + (isServer) ? "server" : "client")
                  break
          }
      } else if (! isServer) {
          switch (trueData.type) {
              case "changeEvent":
                  changeLog.push(trueData.data.newMarkdown)
                  handleChanges(changeLog)
                  break
              case "new-user":
                  answer = prompt("Do you want " + trueData.data.username + " to be part of this collab session? (yes/no)") || "no"

                  response = false

                  switch (answer) {
                      case "yes":
                          response = true
                      case "no":
                          response = false
                      default:
                          response = false
                  }

                  me.messageQueue.push({
                      "type": "voteAnswer",
                      "data": {
                          "allowUser": response
                      }
                  })
                  break
              default:
                  console.log(trueData.type, " Does not have any rules for handling with the " + (isServer) ? "server" : "client" )
                  break
          }
      }

  } catch (error) {
      console.error(error.message)
  }
}

function initalise() {
  message = {
      type: "new-user",
      data: {
          "username": me.username,
      }
  }
}

function beClient (peerId) {
  var peer = new Peer();

  peer.on('open', function (id) {
      console.log('My peer ID is: ' + id);
      conn = peer.connect(peerId);

      console.log("Connecting to ", peerId)

      conn.on('open', function () {
          // Receive messages
          console.log("opened connection")
          conn.on('data', function (data) {
              console.log(data)

              handleData(data, false)
          });

          if (me.isNew) {
              initalise()
          }

          // New message handler
          // Will execute myCallback every 0.5 seconds 
          var intervalID = window.setInterval(handleMessageQueue, 500);

          function handleMessageQueue() {
              for (message in me.messageQueue){
                  conn.send(me.messageQueue[message])
              }

              me.messageQueue = []
          }
      });
  });
}

function startServer() {
  peer = new Peer();

  peer.on('open', function (id) {

      console.log('My peer ID is: ' + id);

      me.peerId = id

      me.isServer = true

      peer.on('connection', function (conn) {
          // Receive messages
          console.log("oh, this is exiting, there's a connection!")

          conn.on('data', function (data) {
              console.log(data)
              handleData(data, true)
          });

          // Send messages
          conn.send('Hello!');

          // New message handler
          // Will execute myCallback every 0.5 seconds 
          var intervalID = window.setInterval(handleMessageQueue, 500);

          function handleMessageQueue() {
              for (message in server.messageQueue){
                  conn.send(server.messageQueue[message])
              }

              server.messageQueue = []
          }

      });

      // THE BELOW CODE IS CURSED: 56INDEPENDENT AND OTHER PEERDOWN CONTRIBUTORS DO NOT ACCEPT ANY RESPONSIBILITY FOR ANY DAMAGE CAUSED BY THIS CODE.
      writePeerId()
      console.log(me.peerId)
      beClient(me.peerId)
  });

}
