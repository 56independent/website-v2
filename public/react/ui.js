// Handles the screen and buttons, alongside their movements.

// Thanks to ChatGPT for this
function copyToClipboard(text) {
    // Create a temporary input element
    var tempInput = document.createElement("input");
  
    // Set the input value to the text you want to copy
    tempInput.value = text;
  
    // Append the input element to the DOM
    document.body.appendChild(tempInput);
  
    // Select the input value
    tempInput.select();
  
    // Copy the selected text to the clipboard
    document.execCommand("copy");
  
    // Remove the temporary input from the DOM
    document.body.removeChild(tempInput);
  }
  

function lobby(goInside) {
    if (goInside) {
        $("#lobby").show()
        $("#game").hide()
    } else {
        $("#lobby").hide()
        $("#game").show()
    }
}

function giveId(id) {
    memory.id = id

    $("#mainArea")
        .append(
            $("<p>")
                .html("Your key is: <code>" + id + "</code>"),
            $("<button>")
                .addClass("btn")
                .text("Copy")
                .click(function () {
                    copyToClipboard(id)
                    $(this).text("Copied!") 
                })
            )

    copyToClipboard(id)
}
