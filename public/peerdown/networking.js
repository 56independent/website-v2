/*
Communication diagram:

sequenceDiagram
    participant ClientServer
    participant Server
    participant Client
    note left of ClientServer: Prepares frontend
    ClientServer ->> Server: I'm ready!
    note left of Server: Prepares services
    Server ->> ClientServer: Ok, now send me some details.
    ClientServer ->> Server: Here they are!
    note left of ClientServer: Sends peer ID to client through other means
    Client ->> Server: Hi! I got the peerID! I'm connected!
    Server ->> ClientServer: Ok so "Client" is connecting. Do you allow them?
    alt fine
        ClientServer ->> Server: Yup, they're fine!
        Server ->> Client: Cool, here is the info you need!
        ClientServer ->> Server: Ok, i just wrote some text
        Note left of Server: Server adds the changes to the log
        Server ->> Client: Some text has been written!
        Server ->> Client: This is the latest change-log!
        note right of Client: Client renders the changes
    else notFine
        ClientServer ->> Server: Nope!
        Server ->> Client: Go away - you're not allowed here!
        Note right of Client: Cries all night now
    end
*/

function handleData(data, isServer) {
    var trueData = {}

    console.log(data)
    try {
        trueData = JSON.parse(data)
        console.log(trueData)

        if (isServer) {
            switch (trueData.type) {
                case "change":
                    server.messageQueue.push(JSON.stringify({
                        "type":"changeEvent",
                        "data": trueData.data
                    }))

                    server.markdown = trueData.data.markdown
                    break
                case "new-user":
                    server.messageQueue.push(JSON.stringify({
                        "type":"initData",
                        "data": {
                            "document": server.markdown
                        }
                    }))
                    break
                case "usernameChange":
                    server.messageQueue.push(JSON.stringify({
                        "type":"changeDisplayedName",
                        "data": trueData.data
                    }))
                    break
                case "ping":
                    server.messageQueue.push(JSON.stringify({
                        "type":"ping",
                        "data": trueData.data
                    }))
                    break
                case "pong":
                    server.userNames.push(trueData.data.username)

                    server.messageQueue.push(JSON.stringify({
                        "type":"user-list",
                        "data": {
                            "nickList":server.userNames
                        }
                    }))
                    break
                default:
                    console.log(trueData.type, " Does not have any rules for handling with the " + (isServer) ? "server" : "client")
                    break
            }
        } else if (! isServer) {
            switch (trueData.type) {
                case "changeDisplayedName":
                    prompt(trueData.data.past + " has changed their name to " + trueData.data.now)
                    break
                case "initData":
                    if (me.isNew) {
                        me.markdown = trueData.data.document
                        $("#markdown-entry").val(me.markdown)
                        renderMarkdown($("#markdown-entry").val())
                    }
                    break
                case "ping":
                    returnMessage = {
                        "type":"pong",
                        "data":{
                            "username": me.username
                        }
                    }
                case "user-list":
                    me.userList = trueData.data.nickList
                    renderUsers(me.userList)
                case "changeEvent":
                    changeLog.push(trueData.data)
                    handleChanges(changeLog)
                    break
                default:
                    console.log(trueData.type, " Does not have any rules for handling with the " + (isServer) ? "server" : "client" )
                    break
            }
        }

    } catch (error) {
        console.error(error.message)
    }
}

function initalise() {
    message = {
        type: "new-user",
        data: {
            "username": me.username,
        }
    }

    me.messageQueue.push(JSON.stringify(message))

    throwPing()
}

function beClient (peerId) {
    var peer = new Peer();

    peer.on('open', function (id) {
        console.log('My peer ID is: ' + id);
        conn = peer.connect(peerId);

        console.log("Connecting to ", peerId)

        conn.on('open', function () {
            // Receive messages
            console.log("opened connection")
            conn.on('data', function (data) {
                console.log(data)

                handleData(data, false)
            });

            if (me.isNew) {
                initalise()
            }

            // New message handler
            // Will execute myCallback every 0.5 seconds 
            var intervalID = window.setInterval(handleMessageQueue, 500);

            function handleMessageQueue() {
                for (message in me.messageQueue){
                    conn.send(me.messageQueue[message])
                }

                me.messageQueue = []
            }
        });
    });
}

function startServer() {
    peer = new Peer();

    peer.on('open', function (id) {

        console.log('My peer ID is: ' + id);

        me.peerId = id

        me.isServer = true

        peer.on('connection', function (conn) {
            // Receive messages
            console.log("oh, this is exiting, there's a connection!")

            conn.on('data', function (data) {
                console.log(data)
                handleData(data, true)
            });

            // Send messages
            conn.send('Hello!');

            // New message handler
            // Will execute myCallback every 0.5 seconds 
            var intervalID = window.setInterval(handleMessageQueue, 500);

            function handleMessageQueue() {
                for (message in server.messageQueue){
                    conn.send(server.messageQueue[message])
                }

                server.messageQueue = []
            }

        });

        // THE BELOW CODE IS CURSED: 56INDEPENDENT AND OTHER PEERDOWN CONTRIBUTORS DO NOT ACCEPT ANY RESPONSIBILITY FOR ANY DAMAGE CAUSED BY THIS CODE.
        writePeerId()
        console.log(me.peerId)
        beClient(me.peerId)
    });

}
