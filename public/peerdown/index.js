// Central databases
let server = {
    messageQueue: [],
    userNames: [],
    voteAnswers: [],
}

let me = {
    username: "",
    isServer: false,
    markdown: "",
    peerId: "",
    isNew: false,
    userList: [],
    messageQueue: []
}

let changeLog = [] // Copies of the markdown.

function copyToClipboard(text) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}

function renderMarkdown(text) {
    var converter = new showdown.Converter({tables: true});
    return converter.makeHtml(text);
}

function publishMarkdown(text) {
    let serverMessage = {
        type: "change",
        data: {
            "markdown": text,
            "nickname": me.username
        }
    }

    me.messageQueue.push(JSON.stringify(serverMessage))
}

function handlePeerId(){
    peerId = getFromURL("peerId") || false

    if (! peerId) {
        actuallyStart = prompt("Start server? (y/N)") || "n"

        start = (actuallyStart = "y") ? true : false

        if (start) {
            startServer()
        }
    } else {
        me.isNew = true
        beClient(peerId)
    }
}

function throwPing(){
    message = {
        "type":"ping",
        "data":{}
    }

    me.messageQueue.push(JSON.stringify(message))
}

handlePeerId()

me.username = getFromURL("username") || "no-name-set-so-is-doofus"
$("#username").val(me.username)
