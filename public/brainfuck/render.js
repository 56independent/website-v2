// Code concerning changes of data on main page

buttonStates = ["btn-scheme-original", "btn-mem-overflow", 100, "btn-hex"]

function queryStates() {
    console.log(buttonStates)
    return buttonStates
}

$(document).ready(function() {
    $("#code").val(getFromURL(code))

    function toggle(self, index, memoryReset) {
        memoryReset = memoryReset || false
        const siblings = self.siblings();
    
        siblings.removeClass("btn-depressed");
        siblings.attr("disabled", false)
        self.addClass("btn-depressed");
        self.attr("disabled", true);

        buttonStates[index] = self.attr("id");

        if (memoryReset) {
            initialiseMemory()
        }
    }

    $("#btn-scheme-mine").click(function() {
        const self = $(this);
        toggle(self, 0)
    })

    $("#btn-scheme-original").click(function() {
        const self = $(this);
        toggle(self, 0)
    })



    $("#btn-mem-overflow").click(function() {
        const self = $(this);
        toggle(self, 1)
    })

    $("#btn-mem-dont").click(function() {
        const self = $(this);
        toggle(self, 1)
    })

    $("#btn-mem-error").click(function() {
        const self = $(this);
        toggle(self, 1)
    })

    $("#speed").change(function () {
        const value = $(this).val()

        buttonStates[2] = value
    })

    $("#code").on("input", function () {
        putInURL("code", $(this).val())
    })

    $("#btn-hex").click(function () {
        const self = $(this);
        toggle(self, 3, false)
    })

    $("#btn-bin").click(function () {
        const self = $(this);
        toggle(self, 3, false)
    })

    $("#btn-dec").click(function () {
        const self = $(this);
        toggle(self, 3, false)
    })

    $("#btn-ascii").click(function () {
        const self = $(this);
        toggle(self, 3, false)
    })
})