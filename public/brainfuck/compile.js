function checkMatchyNess(code, originalOnly) {
    function equalCommonness(char1, char2, string){
        // occurenceNumber = string.match(char1) - string.match(char2)

        // equal = (occurenceNumber == 0) ? true : false

        // return equal

        return true
    }

    if (originalOnly) {
        if (equalCommonness("\\[", "\\]", code)) {
            return true
        } else {
            sendError("You mismatched the [] characters!")
        }
    } else {
        char1s = ["\\[", "\\(", "\\{", "\\\"", "\\*"]
        char2s = ["\\]", "\\)", "\\}", "\\\"", "\\*"]

        for (i = 0; i < char1s.length; i++) {
            if (!equalCommonness(char1s[i], char2s[i], code)) {
                sendError(char1s[i].replace("\\", "") + " is either not present or too present!")
            }
        }
    }
}

function processMine(string) {
    /*
    My version is basically a set of macros which makes programming easier.

    ( ) - Defines a function, which acts like a macro in c
 
    { } - Names the cell we're in and when called later, moves us to that cell.
 
    " " - Defines a string, which will then be placed into the cells from current pointer to the end of input. The beggining and end are in {beggining} and {end}
 
    * * - Defines a block of code which does a meaningful thing
 
    +10 - Repeats plus 200 times into the current pointer location

    We can use Regex to handle this set of macros. This is the order of filtration:

    - Repeats
    - Functions
    - Strings
    - Cell references
    */

    // Process repeats
    repeatCalls = string.match(/\d+/g)


}

// Massive thanks to ChatGPT for generating a large part of the interpreter

function interpretMain(code) {
  const memory = new Uint8Array(30000); // Brainfuck memory with 30,000 cells
  let ptr = 0; // Memory pointer

  
  for (let i = 0; i < code.length; i++) {
      console.log(ptr)

    const instruction = code[i];

    switch (instruction) {
      case '>':
        ptr++;
        break;
      case '<':
        ptr--;
        break;
      case '+':
        memory[ptr]++;
        break;
      case '-':
        memory[ptr]--;
        break;
      case '.':
        if ($("#outputArea").text() == "Code now running (it's very slow!)") {
            $("#outputArea").text("") // This feels like a hack
        }

        $("#outputArea").text($("#outputArea").text() + String.fromCharCode(memory[ptr]));
        break;
      case ',':
        // Input is not supported in this example
        break;
      case '[':
        if (memory[ptr] === 0) {
          let loopCount = 1;
          while (loopCount > 0) {
            i++;
            if (code[i] === '[') loopCount++;
            if (code[i] === ']') loopCount--;
          }
        }
        break;
      case ']':
        if (memory[ptr] !== 0) {
          let loopCount = 1;
          while (loopCount > 0) {
            i--;
            if (code[i] === ']') loopCount++;
            if (code[i] === '[') loopCount--;
          }
        }
        break;
      default:
        // Ignore unrecognized characters
        break;
    }

    updateMemory(memory)
  }

  $("#run").attr("disabled", false);
}