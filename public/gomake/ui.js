function writeScenes() {
    scenes = $("#scenelist")

    scenes.empty()

    list = $("<ul>")

    for (scene in PROJECT.editors) {
        console.log(scene)
        list.append(
            $("<li>")
                .text(scene)
                .click(function() {
                    let ourScene = this.textContent
                    PROJECT.settings.currentObj = ourScene
                    openEditor(PROJECT.editors[ourScene])

                    console.log(ourScene)
                })
        )
    }

    scenes.append(list)
}

function writeClasses(classList, address) {
    // TODOD: Program this damned function

    // classArea = $("#classes")

    // for (i = 0; i < classList.length; i++) {
    //     classArea.append(
    //         $("<input type=\"text\"")
    //             .val(classList[i])
    //             .on("input", () => {
    //                 setDataByAddress(PROJECT.editors[PROJECT.settings.currentObj], address + "class")
    //             })
    //     )
    // }
}

function showCallbacks(address, itemName) {
    callbackArea = $("#callbacks")
    callbackArea.empty()

    list = $("<ul>")

    for (callback in possibleCallbacks) {
        list.append(
            $("<li>")
                .append(
                    $("<code>")
                        .text((PROJECT.settings.advancedMode) ? callback : possibleCallbacks[callback].description)
                        .attr("title", (PROJECT.settings.advancedMode) ? possibleCallbacks[callback].description : callback)
                        .click(function () {
                            editScript(PROJECT.settings.preferredCode, address, (PROJECT.settings.advancedMode) ? $(this).text() : $(this).attr("title"), itemName)
                        })
                )
            )
    }

    callbackArea.append(list)
}

function makeTextScriptEditor(text) {
    var editor = ace.edit("editor");

    editor.setTheme("ace/theme/monokai");
    editor.session.setMode("ace/mode/javascript");
    editor.setFontSize(20)

    editor.setValue(text)

    return editor
}

function editScript(type, address, callback, item) {
    bigAddress = address + ".code." + callback

    $("#callback").empty().append(
        $("<p>").text(callback) // TODO: replace with dropdown showing available callbacks.
    )

    $("#technical-name").empty().append(
        $("<code>").text("$(\"#" + item + "\").on(\"" + callback + "\", () => {").attr("title", possibleCallbacks[callback] || "<not documented>")
    )

    text = getDataByAddress(PROJECT.editors[PROJECT.settings.currentObj], bigAddress)

    if (type == "text"){ // TODO: add Blockly support
        editor = makeTextScriptEditor(text)
        
        $("#save-code").click(() => {
            console.log(editor.getValue())
            console.log(bigAddress)

            // Check that the code object exists. If not, make it exist.

            if (getDataByAddress(address + ".code") != "object") {
                console.log(address + " has no code object")
                PROJECT.editors[PROJECT.settings.currentObj] = setDataByAddress(PROJECT.editors[PROJECT.settings.currentObj], address + ".code", {callback: ""})
            }

            PROJECT.editors[PROJECT.settings.currentObj] = setDataByAddress(PROJECT.editors[PROJECT.settings.currentObj], bigAddress, editor.getValue())
            drawComponents(PROJECT.editors[PROJECT.settings.currentObj])
        })
    }
}

function showParameters(name, components){
    address = findObjectAddressByName(components, name) + name

    console.log(components)

    var workWith = getDataByAddress(components, address);

    console.log(address, workWith)

    $("#paramsSelected").remove()

    addTo = $("<table>")

    for (attribute in attributesObject){
        if (attribute != "class") {
            tooltip = attributesObject[attribute]["tooltip"] || ""

            var row = $("<tr>")
            row.append($("<td>").text((PROJECT.settings.advancedMode) ? attribute : tooltip).attr("title", (PROJECT.settings.advancedMode) ? tooltip : attribute))

            var input = $("<input type=\"text\">")
                        .attr("id", attribute)

            if (! workWith.hasOwnProperty(attribute)) {

                            input.on("input", function () {
                                const attr = $(this).attr("id")

                                workWith[attr] = $(this).val()

                                //console.log($(this).val())

                                //console.log(workWith)
                                PROJECT.editors[PROJECT.settings.currentObj] = setDataByAddress(components, address, workWith)

                                //console.log(PROJECT.editors[PROJECT.settings.currentObj])
                                drawComponents(PROJECT.editors[PROJECT.settings.currentObj])
                            })
            } else {
                input
                    .val(workWith[attribute])
                    .on("input", function () {
                        const attr = $(this).attr("id")

                        workWith[attr] = $(this).val()

                        //console.log($(this).val())

                        //console.log(workWith)
                        PROJECT.editors[PROJECT.settings.currentObj] = setDataByAddress(components, address, workWith)

                        //console.log(PROJECT.editors[PROJECT.settings.currentObj])
                        drawComponents(PROJECT.editors[PROJECT.settings.currentObj])
                    })
            }

            row.append($("<td>").append(input))
            addTo.append(row)
        } else {
            writeClasses(workWith["class"] || [], address)
        }
    }

    $("#params")
        .append(
                $("<div>")
                    .attr("id", "paramsSelected")
                    .append(addTo)
                )

    // TODO: this function has been squashed into a place where it work, but not beautiful

    showCallbacks(address, name)
}

function drawComponents(components) {
    html = renderHTML(components)
    $("#view").empty()
    $("#view").html("<iframe></iframe>").css({
        "width": "100%",
        "height": "75vh"
    });
    var iframe = $("#view").find("iframe")[0].contentWindow.document;
    iframe.open();
    iframe.write(html);
    iframe.close();
}

function selectComponent(name, components){
    console.log("Asked select")
    showParameters(name, components)
}

function makeList(type, things) {
    const allThings = things

    function addSubListItems(things, componentToAddTo, type, depth) {
        let nameOfPlace = type + "-list-"
        let place = $("<div>").attr("id", nameOfPlace + depth)

        for (thing in things) {
            place.append(
                    $("<p>")
                        .text(thing)
                        .click(
                            function () {
                                const requested = this.textContent
                                console.log("Pressed!")
                                console.log(requested)
                                selectComponent(requested, allThings)
                            })
                        .css("margin-left", depth*10)
                )

            if (things[thing].hasOwnProperty('contains') ){
                for (subItem in things[thing].contains) {
                    let buttonName = thing + "-" + depth

                    place
                        .prepend(
                            $("<img>")
                                .attr("src", "caret-up.svg")
                                .attr("id", buttonName)
                                .click(() => {
                                    image = $("#" + buttonName)

                                    opened = (image.attr("src") == "caret-up") ? true : false

                                    elementsOwned = $("#" + nameOfPlace + depth+1)

                                        if (opened) {
                                        elementsOwned.slideUp()
                                        image.attr("src", "caret-down.svg")
                                    } else  {
                                        elementsOwned.slideDown()
                                        image.attr("src", "caret-up.svg")
                                    }
                                })
                            )

                    console.log(things[thing])

                    try {
                        addSubListItems(things[thing]["contains"], place, type, depth+1)
                    } catch (error) {
                        console.trace(error.msg)
                    }
                }
            }
            componentToAddTo.append(place)
        }
    }

    componentToAddTo = (type == "component") ? $("#editors") : (type == "parameter") ? $("#params") : (type == "complexParameter") ? $("#specialParams") : $("#errorLog")

    $("#" + type + "-list-0").remove()

    addSubListItems(things, componentToAddTo, type, 0)
}

function openEditor( things) {
    writeScenes()

    makeList("component", things)

    makeTextScriptEditor("// Add some code here!")
    drawComponents(things)
}
