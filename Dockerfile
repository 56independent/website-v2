FROM nginx:alpine

# Copy configuration files
COPY conf /etc/nginx/nginx.conf

# Copy web files
COPY public /usr/share/nginx/html

# Set file permissions
RUN apk add --no-cache wget \
    && chown -R nginx:nginx /usr/share/nginx/html \
    && chown -R nginx:nginx /etc/nginx/nginx.conf \
    && chmod -R a+r /usr/share/nginx/html

# Expose port 80
EXPOSE 80

CMD ["sh", "-c", "nginx -g 'daemon off;' & while true; do wget -O /usr/share/nginx/html/56i-mtserver-wiki/index.html https://gitlab.com/56independent/wiki/-/raw/master/index.html; sleep 300; done"]